﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GogiBooks.ViewModels.Manage
{
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}
